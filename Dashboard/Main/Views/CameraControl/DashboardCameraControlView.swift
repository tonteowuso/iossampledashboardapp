//
//  CameraControlView.swift
//  Dashboard
//
//  Created by Tonte Owuso on 28/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
import UIKit

// This view follows the Single Responsibility Principle because it has responsibility over a single part of that program's functionality, which it encapsulates

class DashboardCameraControlView:UIView{
    let profilePictureImage: UIImageView = UIImageView()
    let buttonStackView = UIStackView()
    let captureButton = UIButton()
    let cancelButton = UIButton()
    var delegate:DashboardCameraControlViewDelegate?
    let defaultImage = UIImage(named: "profile")
    
    convenience init(delegate: DashboardCameraControlViewDelegate?) {
        
        // this demonstrates dependency injection because the delegate is being injected into the class during initialization
        
        self.init(frame: CGRect.zero)
        self.delegate = delegate
        self.createViews()
    }
    
    func createViews(){
        createProfileImageView()
        createButtons()
    }
    
    func createProfileImageView(){
        profilePictureImage.image = defaultImage
        profilePictureImage.contentMode = .scaleAspectFit
        
        addSubview(profilePictureImage)
        
        profilePictureImage.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(layoutMarginsGuide).offset(50)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().inset(20)
            //            make.edges.equalToSuperview()
        }
        
    }
    
    func createButtons(){
        buttonStackView.distribution = .fillEqually
        buttonStackView.alignment = .fill
        buttonStackView.spacing = 30
        buttonStackView.backgroundColor = UIColor.black
        
        addSubview(buttonStackView)
        
        buttonStackView.snp.makeConstraints{ (make) -> Void in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(layoutMarginsGuide)
            make.height.equalTo(50)
            make.top.equalTo(profilePictureImage.snp.bottom)
            //            make.top.greaterThanOrEqualTo(profilePictureImage.snp.bottom)
        }
        captureButton.setTitle("Capture", for: .normal)
        captureButton.addTarget(self, action: #selector(didTapCaptureButton), for: .touchUpInside)
        captureButton.setTitleColor(UIColor.systemBlue, for: .normal)
        buttonStackView.addArrangedSubview(captureButton)
        cancelButton.setTitle("Cancel", for: .normal)
        captureButton.addTarget(self, action: #selector(didTapCancelButton), for: .touchUpInside)
        cancelButton.setTitleColor(UIColor.systemBlue, for: .normal)
        buttonStackView.addArrangedSubview(cancelButton)
        
    }
    
    
    @objc func didTapCaptureButton(){
        delegate?.didTapCaptureButton()
    }
    
    @objc func didTapCancelButton(){
        delegate?.didTapCancelButton()
    }
    
    func updateData(image:UIImage){
        self.profilePictureImage.image = image
    }
    
    func resetImage(){
        DispatchQueue.main.async {
            self.profilePictureImage.image = nil
            self.profilePictureImage.image = self.defaultImage
            self.profilePictureImage.setNeedsDisplay()
        }
    }
    
}
