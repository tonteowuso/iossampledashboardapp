//
//  DashboardColumnChartViewProtocol.swift
//  Dashboard
//
//  Created by Tonte Owuso on 27/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation

// This protocol follows the Interface Segregation Principle because delegates are not forced to depend on methods they do not use

protocol DashboardColumnChartViewDelegate: class {
    func sliderDidChange()
}

