//
//  DashboardColumnChartView.swift
//  Dashboard
//
//  Created by Tonte Owuso on 27/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
import UIKit
import Charts

// This view follows the Single Responsibility Principle because it has responsibility over a single part of that program's functionality, which it encapsulates

class DashboardColumnChartView:UIView{
    let columnChartView:BarChartView = BarChartView()
    var delegate:DashboardColumnChartViewDelegate?
    var slider:UISlider = UISlider()
    var activityIndicator = UIActivityIndicatorView()
    var viewModel:DashboardColumnChartViewModel?
    
    convenience init(delegate: DashboardColumnChartViewDelegate?) {
        self.init(frame: CGRect.zero)
        // this demonstrates dependency injection because the delegate is being injected into the class during initialization
        self.delegate = delegate
        self.createViews()
        self.viewModel = DashboardColumnChartViewModel(view: self)
    }
    
    func createViews(){
        createSlider()
        createColumnChart()
        createActivityIndicator()
    }
    
    func createSlider(){
        slider.minimumValue = 0
        slider.maximumValue = 5
        slider.isContinuous = false
        slider.addTarget(self, action: #selector(sliderValueDidChange), for: .valueChanged)
        self.addSubview(slider)
        slider.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(layoutMarginsGuide)
            make.width.equalTo(100)
            make.centerX.equalToSuperview()
        }
    }
    
    func createColumnChart(){
        self.addSubview(columnChartView)
        columnChartView.snp.makeConstraints{ (make) -> Void in
            make.bottom.equalTo(slider.snp.top)
            make.left.right.equalToSuperview()
            make.top.equalTo(layoutMargins).offset(50)
        }
    }
    
    func createActivityIndicator(){
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .medium
        activityIndicator.color = UIColor.black
        self.addSubview(activityIndicator)
        
        activityIndicator.snp.makeConstraints{ (make) -> Void in
            make.center.equalTo(slider.snp.center)
        }
    }
    
    func showLoading(){
        slider.isHidden = true
        activityIndicator.startAnimating()
    }
    
    func hideLoading(){
        slider.isHidden = false
        activityIndicator.stopAnimating()
    }
    
    func errorOnLoadingData(){
        hideLoading()
    }
    
    @objc func sliderValueDidChange(){
        showLoading()
        delegate?.sliderDidChange()
    }
    
    func updateData(teams:[Team]){
        viewModel?.updateData(teams: teams)
    }
}
