//
//  DashboardColumnChartViewModel.swift
//  Dashboard
//
//  Created by Tonte Owuso on 28/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
import Charts

class DashboardColumnChartViewModel{
    var entries:[BarChartDataEntry] = []
    
    var view:DashboardColumnChartView
    
    init(view:DashboardColumnChartView) {
        self.view = view
    }
    
    func updateData(teams:[Team]){
        view.hideLoading()
        self.entries.removeAll()
        for (index,team) in teams.enumerated() {
            let entry = BarChartDataEntry(x: Double(index), y: Double(team.wins))
            entries.append(entry)
        }
        let dataSet = BarChartDataSet(entries: entries)
        let data = BarChartData(dataSet: dataSet)
        dataSet.colors = ChartColorTemplates.pastel()
        view.columnChartView.data = data
        view.columnChartView.notifyDataSetChanged()
    }
}
