//
//  ChartView.swift
//  Dashboard
//
//  Created by Tonte Owuso on 27/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
import UIKit
import Charts
import SnapKit

// This view follows the Single Responsibility Principle because it has responsibility over a single part of that program's functionality, which it encapsulates

class DashboardPieChartView:UIView{
    let pieChartView:PieChartView = PieChartView()
    var delegate:DashboardPieChartViewDelegate?
    var stepper:UIStepper = UIStepper()
    var activityIndicator = UIActivityIndicatorView()
    
    var viewModel:DashboardPieChartViewModel?
    
    convenience init(delegate: DashboardPieChartViewDelegate?) {
        
        // this demonstrates dependency injection because the delegate is being injected into the class during initialization
        
        self.init(frame: CGRect.zero)
        self.delegate = delegate
        self.createViews()
        self.viewModel = DashboardPieChartViewModel(view: self)
    }
    
    func createViews(){
        createStepper()
        createPieChart()
        createActivityIndicator()
    }
    
    func createPieChart(){
        
        pieChartView.holeColor = UIColor.clear
        pieChartView.chartDescription?.textColor = UIColor.white
        pieChartView.backgroundColor = UIColor.clear
        
        self.addSubview(pieChartView)
        
        pieChartView.snp.makeConstraints{ (make) -> Void in
            make.top.equalToSuperview()
            make.bottom.equalTo(stepper.snp.top).offset( -50)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().inset(20)
        }
    }
    
    func createStepper(){
        stepper.tintColor = UIColor.blue
        stepper.addTarget(self, action: #selector(didTapStepper), for: .touchUpInside)
        self.addSubview(stepper)
        
        stepper.snp.makeConstraints{ (make) -> Void in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(layoutMarginsGuide)
        }
    }
    
    func createActivityIndicator(){
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .medium
        activityIndicator.color = UIColor.black
        self.addSubview(activityIndicator)
        
        activityIndicator.snp.makeConstraints{ (make) -> Void in
            make.center.equalTo(stepper.snp.center)
        }
    }
    
    func showLoading(){
        stepper.isHidden = true
        activityIndicator.startAnimating()
    }
    
    func hideLoading(){
        stepper.isHidden = false
        activityIndicator.stopAnimating()
    }
    
    func updateData(teams: [Team]){
        viewModel?.updateData(teams: teams)
    }
    
    func errorOnLoadingData(){
        hideLoading()
    }
    
    @objc func didTapStepper(){
        showLoading()
        delegate?.didTapStepper()
    }
    
    
    
}
