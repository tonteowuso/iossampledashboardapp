//
//  DashboardPieChartViewModel.swift
//  Dashboard
//
//  Created by Tonte Owuso on 28/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
import Charts

class DashboardPieChartViewModel{
    var entries:[PieChartDataEntry] = []
    var view:DashboardPieChartView
    
    init(view:DashboardPieChartView) {
        self.view = view
    }
    
    func updateData(teams:[Team]){
        view.hideLoading()
        self.entries.removeAll()
        for team in teams {
            let entry = PieChartDataEntry(value: Double(team.wins), label: team.country)
            entries.append(entry)
        }
        let dataSet = PieChartDataSet(entries: entries)
        let data = PieChartData(dataSet: dataSet)
        dataSet.colors = ChartColorTemplates.pastel()
        view.pieChartView.data = data
        view.pieChartView.notifyDataSetChanged()
    }
    
}
