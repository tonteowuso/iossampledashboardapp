//
//  MainDashboardRepository.swift
//  Dashboard
//
//  Created by Tonte Owuso on 28/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
import UIKit

class MainDashboardViewModel{
    var view: MainDashboardViewController
    var  webRequest = WebRequest()
    init(view:MainDashboardViewController) {
        self.view = view
    }
    
    func getTeams(completion: @escaping ((Result<[Team],Error>) -> Void)){
        webRequest.load(url: URLS.fetchTeamResults, completion: {
            (result) in
            switch result{
            case .success(let data):
                do {
                    let items = try JSONDecoder().decode([Team].self, from: data)
                    completion(.success(items))
                } catch {
                    completion(.failure(error))
                }
            case .failure(let error):
                print("error retrieving teams " + error.localizedDescription)
                completion(.failure(error))
            }
        })
    }
    
    func loadData(){
        loadPieChartData()
        loadBarChartData()
    }
    
    func loadPieChartData(){
        self.getTeams(completion:{ (result) in
            switch result {
            case .success(let teams):
                if let pieChart = self.view.pieChartView {
                    pieChart.updateData(teams: teams)
                }
            case .failure(let error):
                print("error loading team data: \(error)")
                self.view.columnChart?.errorOnLoadingData()
            }
        })
    }
    
    
    func loadBarChartData(){
        self.getTeams(completion:{ (result) in
            switch result {
            case .success(let teams):
                if let barChart = self.view.columnChart {
                    barChart.updateData(teams: teams)
                }
            case .failure(let error):
                print("error loading team data: \(error)")
                self.view.columnChart?.errorOnLoadingData()
            }
        })
    }
    
    func resetCameraControlImage(){
        view.cameraControlView?.resetImage()
    }
    
    func imagePicked(info: [UIImagePickerController.InfoKey : Any]){
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            view.cameraControlView?.updateData(image: image)
        }
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            view.cameraControlView?.updateData(image: image)
        }
        
        
        
    }
}
