//
//  ViewController.swift
//  Dashboard
//
//  Created by Tonte Owuso on 27/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import UIKit

// This class follows the Open Closed Principle because additional functionality has been added through extensions instead of modifying it directly

class MainDashboardViewController: UIViewController {
    
    var mainStackView:UIStackView = UIStackView()
    var pieChartView:DashboardPieChartView?
    var columnChart: DashboardColumnChartView?
    var cameraControlView: DashboardCameraControlView?
    var viewModel: MainDashboardViewModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = MainDashboardViewModel(view: self)
        createViews()
        viewModel?.loadData()
    }
    
    func createViews() {
        view.backgroundColor = UIColor.white
        createMainStackView()
        createPieChart()
        createColumnChart()
        createCameraControl()
    }
    
    
    func createMainStackView() {
        self.view.addSubview(mainStackView)
        if UIDevice.current.userInterfaceIdiom == .phone {
            mainStackView.axis = .vertical
        } else {
            mainStackView.axis = .horizontal
        }
        mainStackView.distribution = .fillEqually
        mainStackView.alignment = .fill
        mainStackView.spacing = 5
        mainStackView.snp.makeConstraints{ (make) -> Void in
            make.top.equalTo(self.view.layoutMarginsGuide).offset(50)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(self.view.layoutMarginsGuide).inset(50.0)
        }
    }
    
    func createPieChart() {
        
        pieChartView = DashboardPieChartView(delegate:self)
        if let pieChart = pieChartView{
            self.mainStackView.addArrangedSubview(pieChart)
        }
    }
    
    func createColumnChart() {
        
        columnChart = DashboardColumnChartView(delegate: self)
        if let barChart = columnChart{
            self.mainStackView.addArrangedSubview(barChart)
        }
    }
    
    func createCameraControl(){
        cameraControlView = DashboardCameraControlView(delegate: self)
        if let cameraControl = cameraControlView{
            self.mainStackView.addArrangedSubview(cameraControl)
        }
    }
    
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        if view.traitCollection.verticalSizeClass == .compact {
            mainStackView.axis = .horizontal
        } else {
            mainStackView.axis = .vertical
        }
    }
}



extension MainDashboardViewController:DashboardPieChartViewDelegate {
    
    func didTapStepper() {
        viewModel?.loadPieChartData()
    }
}

extension MainDashboardViewController:DashboardColumnChartViewDelegate {
    
    func sliderDidChange() {
        viewModel?.loadBarChartData()
    }
    
}

extension MainDashboardViewController:DashboardCameraControlViewDelegate {
    func didTapCaptureButton() {
        let vc = UIImagePickerController()
        vc.allowsEditing = true
        vc.delegate = self
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            vc.sourceType = .camera
        }
        else{
            // for simulator use
            vc.sourceType = .photoLibrary
        }
        
        present(vc, animated: true)
       
    }
    
    func didTapCancelButton() {
        viewModel?.resetCameraControlImage()
    }
    
    
}

extension MainDashboardViewController:UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        viewModel?.imagePicked(info:info)
        dismiss(animated: true, completion: nil)
    }
}

