//
//  Team.swift
//  Dashboard
//
//  Created by Tonte Owuso on 27/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
struct Team:Codable{
    var id:Int
    var country:String
    var wins:Int
    var group_id:Int
    var points:Int
    
    //This is a model for each world cup team's results at the FIFa 2018 Women's world cup
        
}
