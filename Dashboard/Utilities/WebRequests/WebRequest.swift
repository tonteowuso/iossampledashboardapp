//
//  WebRequestConnector.swift
//  Dashboard
//
//  Created by Tonte Owuso on 27/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation

class WebRequest {
    
     func load(url: String, completion:@ escaping (Result<Data,Error>) -> ()){
       guard let encodedURL = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {return}
       guard let serviceUrl = URL(string: encodedURL) else {return}
           var request = URLRequest(url: serviceUrl)
           request.httpMethod = "GET"
           let session = URLSession.shared
       
           session.dataTask(with: request) { (data, response, error) in
               print(request)
            
            guard let data = data else {
                if let error = error{
                    completion(.failure(error))
                }
                return
            }
            
            DispatchQueue.main.async {
                completion(.success(data))
            }
               
               }.resume()
           }
}
