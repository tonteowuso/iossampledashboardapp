//
//  URL.swift
//  Dashboard
//
//  Created by Tonte Owuso on 27/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
struct URLS {
    // returns a list of 5 random womens world cup 2018 teams
    static let fetchTeamResults = "https://tonte-test-server.herokuapp.com/teams/results?size=5"
}
