//
//  MainDashboardViewModel.swift
//  DashboardTests
//
//  Created by Tonte Owuso on 28/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//

import Foundation
import XCTest
@testable import Dashboard

class MainDashboardViewModelTest: XCTestCase{
    var mainDashboardViewModel = MainDashboardViewModel(view: MockView())
    
    override func setUp() {
        mainDashboardViewModel = MainDashboardViewModel(view: MockView())
    }
    
    func testGetTeams(){
        mainDashboardViewModel.getTeams(completion: {
            (result) in
            switch result{
            case .success(let data):
                XCTAssertNotNil(data)
            case .failure(_):
                XCTAssert(false)
            }
        })
    }
    
    class MockView:MainDashboardViewController{
        
    }
    
    class MockWebRequest:WebRequest{
        
        // testing a sample web response and if it can be decoded properly
        override func load(url: String, completion: @escaping (Result<Data, Error>) -> ()) {
            let data:[[String:Any]] = [["id":17,"country":"Canada","alternate_name":"","fifa_code":"CAN","group_id":5,"group_letter":"E","wins":2,"draws":0,"losses":2,"games_played":4,"points":6,"goals_for":4,"goals_against":3,"goal_differential":1]]
             do {
             let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                completion(.success(jsonData))
            }
             catch{
                 XCTAssert(false)
            }
            
            
        }
    }
}
