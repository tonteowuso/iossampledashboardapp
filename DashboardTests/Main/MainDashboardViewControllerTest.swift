//
//  MainDashboardViewControllerTest.swift
//  DashboardTests
//
//  Created by Tonte Owuso on 28/08/2020.
//  Copyright © 2020 Tonte Owuso. All rights reserved.
//
import XCTest
@testable import Dashboard
import Foundation

class MainDashboardViewControllerTest: XCTestCase{
    var mainDashboardController = MainDashboardViewController()
    var window = UIWindow()
    
    override func setUp() {
        setupTestViewController()
    }
    
    func setupTestViewController(){
        mainDashboardController = MainDashboardViewController()
        mainDashboardController.viewModel = MockViewModel(view: mainDashboardController)
        window = UIWindow()
        window.addSubview(mainDashboardController.view)
        let _ = mainDashboardController.view
    }
    
    func testViewModelInit(){
        XCTAssertNotNil(mainDashboardController.viewModel)
    }
    
    func testCreateViews(){
        XCTAssertTrue(mainDashboardController.view.backgroundColor == UIColor.white)
        XCTAssertNotNil(mainDashboardController.mainStackView.superview)
        XCTAssertNotNil(mainDashboardController.pieChartView?.superview)
        XCTAssertNotNil(mainDashboardController.columnChart?.superview)
        XCTAssertNotNil(mainDashboardController.cameraControlView?.superview)
    }
    
    func testTraitCollectionDidChange(){
        mainDashboardController.traitCollectionDidChange(.init(horizontalSizeClass: .regular))
        XCTAssertTrue(mainDashboardController.mainStackView.axis == .vertical)
    }
    
    func testDidTapStepper(){
        mainDashboardController.didTapStepper()
    }
    func testSliderDidChange(){
        mainDashboardController.sliderDidChange()
    }
    
    
    func testDidTapCaptureButton(){
        mainDashboardController.didTapCaptureButton()
        XCTAssertNotNil(mainDashboardController.view.window)
    }
    
    
    func testDidTapCancelButton() {
         mainDashboardController.didTapCancelButton()
    }
    
    func testImagePicker(){
        mainDashboardController.imagePickerController(UIImagePickerController(), didFinishPickingMediaWithInfo: [UIImagePickerController.InfoKey.originalImage : UIImage(named: "profile") as Any])
    }
    
    
    
    class MockViewModel:MainDashboardViewModel{
        // this is to check if it calls the function correctly. Actual load data functionality will be checked in the view model's own unit test
        
        override func loadData() {
            XCTAssertTrue(true)
        }
        
        override func loadPieChartData() {
             XCTAssertTrue(true)
        }
        
        override func loadBarChartData() {
            XCTAssertTrue(true)
        }
        
        override func resetCameraControlImage(){
            XCTAssertTrue(true)
        }
        
        override func imagePicked(info: [UIImagePickerController.InfoKey : Any]) {
            XCTAssertTrue(true)
        }
        
    }
}
